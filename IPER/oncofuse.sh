#/bin/bash

inputfile=$1
filetype=$2  #cg,tophat,rnastar,coord,fcatcher
tissue=$3
outfile=$4
build=$5
iper_path=$6

### CG junction file conversion
if [ $filetype == "cg" ]
then 
	awk 'BEGIN{
			FS="\t";
			OFS="\t";
			tissue="'"$tissue"'"
		}{
			if(!index($0,"#")==1 && index($0,">")!=1 && $0!="" ){
				print $2,$3+1,$6,$7,tissue		
			}		
	
		}END{}' $inputfile > oncofuse_in
		
		filetype="coord"
else
	cp $inputfile oncofuse_in
fi		


### liftover if needed TODO



### run oncofuse
# java -Xmx1G -jar Oncofuse.jar input_file input_type tissue_type output_file
#
java -Xmx1G -jar ${iper_path}/oncofuse-v1.0.6/Oncofuse.jar oncofuse_in $filetype $tissue $outfile


### join oncofuse results with junctionsfile if CG TODO





