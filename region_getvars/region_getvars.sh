#!/bin/bash

function usage(){
	echo "usage: $0 todo"
}

#default parameter settings
region_hasheader="N"
sample_hasheader="Y"
region_chrcol=1
region_startcol=2
region_endcol=3
sample_chrcol=2
sample_startcol=3
sample_endcol=4
inputfiles=""
labels=""
regionlabel=""

set -- `getopt -n$0 -u -a --longoptions="regionfile: regionlabel: samplefile: region_chrcol: region_startcol: region_endcol: sample_chrcol: sample_startcol: sample_endcol: region_hasheader: region_issorted: label: annotated_regions_out: annotated_variants_out: annotated_variants_id: annotated_variants_path: " "h:" "$@"` || usage
[ $# -eq 0 ] && usage

while [ $# -gt 0 ]
do
    case "$1" in
       	--regionfile)      	regionfile=$2;shift;;  
       	--regionlabel)     	regionlabel=$2;shift;;  
		--samplefile)		inputfiles="$inputfiles,$2";shift;; 
		--label)			labels="$labels,$2";shift;;	  #galaxy specific (filename used when label not provided)
		--region_chrcol)	region_chrcol=$2;shift;;  
		--region_startcol)	region_startcol=$2;shift;;  
		--region_endcol)	region_endcol=$2;shift;;  
		--region_hasheader)	region_hasheader=$2;shift;;
		--region_issorted)	region_issorted=$2;shift;;
		--sample_chrcol)	sample_chrcol=$2;shift;;  
		--sample_startcol)	sample_startcol=$2;shift;;  
		--sample_endcol)	sample_endcol=$2;shift;; 
		--annotated_regions_out)	annotated_regions_final=$2;shift;;  #galaxy specific
		--annotated_variants_out)	annotated_variants_final=$2;shift;; #galaxy specific
		--annotated_variants_id)	annotated_variants_id=$2;shift;;    #galaxy specific
		--annotated_variants_path)	annotated_variants_path=$2;shift;;  #galaxy specific		
        -h)        	shift;;
		--)        	shift;break;;
        -*)        	usage;;
        *)         	break;;            
    esac
    shift
done


#remove first comma in list of inputfiles and labels
inputfiles=${inputfiles:1:${#inputfiles}}
labels=${labels:1:${#labels}}

#if no lable provided, use filenames
#if [ $label_regions == "" ]
#then
#	label_regions=`basename $regionfile`
#fi


#parse varfile list and label list
variantfiles=${inputfiles//,/ }
samplelabels=(${labels//,/ })

echo "variantfiles: $variantfiles samplelables: $samplelabels"

#sort region file?


# for each samplefile, find variants falling in regions
cp $regionfile annotated_regions.tsv
annotated_variantsfile=annotated_variants.tsv
annotated_regionsfile=annotated_regions_out.tsv

#if no header regionfile, add one
if [ $region_hasheader == "N" ]
then
	sed -i '1 { h; s/[^\t]//g; p; g; }' annotated_regions.tsv  #copy first line and insert at beginning, remove all non-tab chars (to clear fields)
else
	region_header=`head -1 $regionfile`
fi

varfile_num=-1
for v in ${variantfiles}
do
	varfile="myvariantfile.txt"
	cp $v $varfile
	varfile_num=$[$varfile_num+1]
	mylabel=${samplelabels[$varfile_num]}
	echo "label of current file ($varfile_num): $mylabel"
	
	#remove header starting with # or empty line (for CG files)
	sed -i '/^#/ d' $varfile
	sed -i '/^$/ d' $varfile
	
	echo "joining regionfile with $varfile"
	head annotated_regions.tsv
	# awk, remove chrprefix: sub(/chr/, "", $chrocol)
	awk 'BEGIN{
		FS="\t"
		OFS="\t"
		regioncount=0
			
	}{
		#first pass remember all regions
		if(FNR==NR){	
			if(FNR==1) {
				print $0, "'"$mylabel"'" > "'"$annotated_regionsfile"'"				
			}
			else{	
				region_entry[regioncount]=$0	#remember entire region line		
				#region_chr[regioncount]=gensub(/chr/, "","g",$"'"$region_chrcol"'")  # remove possible chr prefix
				 chromo=$"'"$region_chrcol"'"
				 sub(/chr/, "",chromo)  # remove possible chr prefix
				 region_chr[regioncount]=chromo+0
				region_start[regioncount]=$"'"$region_startcol"'"+0
				region_end[regioncount]=$"'"$region_endcol"'"+0
				region_varcount[regioncount]=0				
				regioncount++
			}
		}
		
		#second pass find all variants overlapping each region
		#TODO: cutoff search assuming region file is sorted
		else{
			if(FNR==1) {
				if("'"$region_hasheader"'"=="N")
					print $0,"'"$regionlabel"'"	> "'"$annotated_variantsfile"'"
				else	
					print $0,"'"$region_header"'"	> "'"$annotated_variantsfile"'"
			}
			else{
				for(i=0;i<regioncount;i++){				
				
					#chr=gensub(/chr/, "","g",$"'"$sample_chrcol"'")
					chr=$"'"$sample_chrcol"'"
					sub(/chr/, "",chr)
					#if file is sorted, if chr num too high for region, break
					if("'"$region_issorted"'"=="Y" && region_chr[i] ~ /^[0-9]$/ && chr+0 > region_chr[i]+0){
						#print "breaking, region: "region_chr[i]," sample: ",chr
						break
					}				
					
					if(chr==region_chr[i] && $"'"$sample_startcol"'"+0<=region_end[i] && $"'"$sample_endcol"'"+0>=region_start[i] ){ #if region overlaps with current variant
						#print "match found: region: "region_chr[i],region_start[i],region_end[i], " sample: "$"'"$sample_chrcol"'",$"'"$sample_startcol"'",$"'"$sample_endcol"'"
						region_varcount[i]++
						print $0, region_entry[i] > "'"$annotated_variantsfile"'"
					}										
				}
			}		
		}		
			
	}END{
		#create output file
		for(i=0;i<regioncount;i++){
			print region_entry[i],region_varcount[i] > "'"$annotated_regionsfile"'"
		}
	
	
	}' annotated_regions.tsv ${varfile} 
	
	#get ready for next iteration
	rm annotated_regions.tsv
	mv $annotated_regionsfile annotated_regions.tsv
	
	#move annotated variants file of this iteration to galaxy output folder and naming
	echo "moving output files"
	if [ $varfile_num -eq 0 ] #copy first into annotated_variants_final
	then
		cp $annotated_variantsfile ${annotated_variants_final}
	else
		cp $annotated_variantsfile "${annotated_variants_path}/primary_${annotation_variants_id}_AnnotatedVariants${varfile_num}_visible_tabular"
		tree
		
	fi
done


# create final output names
cp annotated_regions.tsv $annotated_regions_final


