##############################################################
#                                                            # 
#    Create fasta file from CG reference crr and varfile     #
#                                                            #
##############################################################

import sys
import commands
import csv,bz2
#import matplotlib.pyplot as pyplot

## change when migrated
cgatools='cgatools18'                       # command/path of cgatools binary
hg18crr='/mnt/compgen/reference/hg18.crr'   # hg18 reference crr file
hg19crr='/mnt/compgen/reference/hg19.crr'   # hg19 reference crr file
comprof='./comprof'                         # command/path of comprof binary (DNA-at-a-glance)

#chromosome lengths
chromlengths_hg18 = {'chr1': 247249719, 'chr2': 242951149, 'chr3': 199501827, 'chr4': 191273063, 'chr5': 180857866,
                     'chr6': 170899992, 'chr7': 158821424, 'chr8': 146274826, 'chr9': 140273252, 'chr10': 135374737,
                     'chr11': 134452384,'chr12': 132349534,'chr13': 114142980,'chr14': 106368585,'chr15': 100338915,
                     'chr16': 88827254, 'chr17': 78774742, 'chr18': 76117153, 'chr19': 63811651, 'chr20': 62435964,
                     'chr21': 46944323, 'chr22': 49691432, 'chrX': 154913754, 'chrY': 57772954}
chromlengths_hg19 = {'chr1': 249250621, 'chr2': 243199373, 'chr3': 198022430, 'chr4': 191154276, 'chr5': 180915260,
                     'chr6': 171115067, 'chr7': 159138663, 'chr8': 146364022, 'chr9': 141213431, 'chr10': 135534747,
                     'chr11': 135006516,'chr12': 133851895,'chr13': 115169878,'chr14': 107349540,'chr15': 102531392,
                     'chr16': 90354753,'chr17': 81195210,  'chr18': 78077248, 'chr19': 59128983,'chr20': 63025520 ,
                     'chr21': 48129895,'chr22': 51304566,  'chrX': 155270560, 'chrY': 59373566}


### Preprocessing

# check arguments
if len(sys.argv) != 4:
    print '\n Usage varfile2fasta.py <reference build> <varfile> <chromosome>'
    print ' --> example ./varfile2fasta.py hg18 varfile.tsv chr22 sample \n'
    sys.exit()        

# parse arguments
reference=sys.argv[1]
varfile=sys.argv[2]
chromosome=sys.argv[3]

# Check chromosome parameter, add 'chr' prefix if missing. 
if 'chr' not in chromosome:
    chromosome='chr'+chromosome
try:
    chromlengths_hg18[chromosome]
except KeyError:
    print "\n ERROR: Invalid chromosome chosen, must be [chr]<1-22,X,Y>. \
          For Example: chr1, chrX, or 22 or Y, etc \n"
    sys.exit()

       
# check reference build    
if reference == 'hg18':
    reference=hg18crr
    chromlengths=chromlengths_hg18
elif reference == 'hg19':
    reference=hg19crr
    chromlengths=chromlengths_hg18
else:
    print "\n ERROR: Invalid reference build, please specify hg18 or hg19. \n"
    sys.exit()
    

print '- reference:  ',reference
print '- varfile:    ',varfile
print '- chromosome: ',chromosome
print ''


# read varfile. if bz2 file, decompress first
if 'bz2' in varfile: 
    print " -> Decompressing bz2 zipped file"
    vfile = bz2.BZ2File(varfile)         
else:
    vfile = open(varfile,'r')
    



### get reference fasta sequence for chromosome from crr file
chrlen=chromlengths[chromosome]
print ' -> Creating reference fasta sequence ('+ cgatools+' decodecrr --reference '+reference+' --range '+chromosome+':0-'+str(chromlengths[chromosome])+' --output reffasta.fa )'
commands.getoutput(cgatools+' decodecrr --reference '+reference+' --range '+chromosome+':0-'+str(chromlengths[chromosome])+' --output reffasta.fa')

### process varfile and create fasta  
print " -> Creating sample fasta sequences"

#outfileName, outfileExtension = os.path.splitext(outfile)
fastafileA1_name='Allele1.fa'
fastafileA2_name='Allele2.fa'

fastafileA1=open(fastafileA1_name,'w')
fastafileA2=open(fastafileA2_name,'w')


fastafileA1.write('> chromosome '+chromosome+', allele 1, of file '+varfile+' using reference genome '+reference+'\n')
fastafileA2.write('> chromosome '+chromosome+', allele 2, of file '+varfile+' using reference genome '+reference+'\n')

seen='N'
lastchr='chromosome'
count=-1
perccount=0
sequence1=''
sequence2=''
for line in csv.reader(vfile,delimiter='\t'):   
    if len(line) == 14: #skip header
        if line[3] != lastchr:
            sys.stdout.write('  '+line[3])
            sys.stdout.flush()
            lastchr = line[3]
            
        if line[3] == chromosome:
            count+= 1
            seen='Y'
            begin = int(line[4])
            end = int(line[5])
            vartype = line[6]
            allele = line[2]
            alleleseq = line[8]            
           
            #after every percentage completion, write sequence to file to avoid using up too much memory
            if count%(int(chrlen/100)) == 0:
                sys.stdout.write('  '+str(perccount)+'%')
                sys.stdout.flush()
                #fastafileA1.write(sequence1)
                #fastafileA2.write(sequence2)
                #sequence1=''
                #sequence2=''
                
            #print "line: ",line
            if vartype == 'no-call' or vartype == 'no-call-ri' or vartype == 'no-call-rc' or vartype == 'no-ref':               
                if allele == '1' or allele == 'all':
                    sequence1='N'*(end-begin)
                if allele == '2' or allele == 'all':
                    sequence2='N'*(end-begin)
                    
            elif vartype == 'ref':                
                if allele == '1' or allele == 'all':                    
                    sequence1=commands.getoutput(cgatools+' decodecrr --reference '+reference+' --range '+chromosome+':'+str(begin)+'-'+str(end))
                if allele == '2' or allele == 'all':
                    sequence2=commands.getoutput(cgatools+' decodecrr --reference '+reference+' --range '+chromosome+':'+str(begin)+'-'+str(end))
               
            elif vartype == 'snp' or vartype == 'ins' or vartype == 'sub':                
                if allele == '1' or allele == 'all':
                    sequence1=alleleseq                   
                if allele == '2' or allele == 'all':
                    sequence2=alleleseq
                    
            elif vartype != 'del':
                 print 'unknown vartype: ',vartype
             
            if allele == '1' or allele == 'all':
                    fastafileA1.write(sequence1)                  
            if allele == '2' or allele == 'all':
                    fastafileA2.write(sequence2)
            
            
        elif line[3] != chromosome and seen == 'Y':
            print " -> Finished processing file (reached chromosome "+line[3]+')'
            break
            
# write to file
#fastafileA1.write(sequence1)
#fastafileA2.write(sequence2)
        
vfile.close()
fastafileA1.close()
fastafileA2.close()        
          


          
            
            
            
            
            
            
            