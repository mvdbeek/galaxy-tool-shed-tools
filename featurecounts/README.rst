FeatureCounts wrapper for Galaxy
================================

http://bioinf.wehi.edu.au/featureCounts/
http://subread.sourceforge.net/

FeatureCounts as part of the SUBREAD package is "a highly efficient and
accurate read summarization program".

Development
-----------

* Repository-Maintainer: Youri Hoogstrate

* Repository-Development: https://bitbucket.org/EMCbioinf/galaxy-tool-shed-tools

The tool wrapper has been written by Youri Hoogstrate from the Erasmus
Medical Center (Rotterdam, Netherlands) on behalf of the Translational
Research IT (TraIT) project:

http://www.ctmm.nl/en/programmas/infrastructuren/traitprojecttranslationeleresearch

More tools by the Translational Research IT (TraIT) project can be found in the following repository:

http://toolshed.dtls.nl/

License
-------

**featureCounts**:

GPL (>=3)

**featurecounts2bed**:

GPL (>=3)

**This wrapper**:

    Copyright (C) 2013-2014  Youri Hoogstrate

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
