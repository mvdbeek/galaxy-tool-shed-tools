#!/bin/env python
import os,sys
import optparse
import math

from delimitedfile import DelimitedFile
from lGamma import Lgamma


def openFileByExtension(fn):
    '''Open a text file that may be compressed.

    Returns the file-like object of appropriate type depending on the file
    extension (.bz2 and .gz are supported).'''

    _, ext = os.path.splitext(fn)
    if ext == '.bz2':
        import bz2
        return bz2.BZ2File(fn, 'r', 2*1024*1024)
    elif ext == '.gz':
        import gzip
        return gzip.GzipFile(fn, 'r')
    else:
        return open(fn, 'r')


## silly values, overridden in main()
normalPvalueCutoff = 1.0
minNormalHetCount = 1000000
maxNormalHetCount = -1

numLAFbins = 50
binLAF = []
logL = []
confidenceInterval = .99

sep = '\t'


mvChr = ""
mvBeg = -1
mvEnd = -1
mvZyg = ""
mvVT = ""
mvA1RC = -1
mvA2RC = -1
mvTRC = -1

winChr = ""
begin = None
winBegin = -1
winEnd = -1
hetCount = 0
allele1CountArray = []
allele2CountArray = []

numFields = 0

inputCnvLine = ''




def logsum(a,b):
    rv = 0.0
    if(a>b):
        rv = a + math.log(1.0+math.exp(b-a))
    else:
        rv = b + math.log(1.0+math.exp(a-b))
    return rv


def my_log_betaBinomProb(n,k,alpha,beta):
    rv =Lgamma(n+1.) -Lgamma(k+1.) -Lgamma(n-k+1.) \
      +Lgamma(alpha+k) +Lgamma(n+beta-k) -Lgamma(alpha+beta+n) \
      +Lgamma(alpha+beta) -Lgamma(alpha) -Lgamma(beta)
    return rv
    

def analytic_calc_alpha_beta(mu,vStar):
    '''calculate alpha and beta parameters to beta distribution (bias distribution) from mean of 
    the desired distribution and the variance of the beta distribution for equal allele fraction
    using original (incorrect) solution for the ratio of variances'''
    v = vStar * 16. * mu * mu * (1.-mu) * (1.-mu)
    alpha = mu * mu * (1.-mu)/v-mu
    beta = alpha * (1.-mu) / mu
    return [alpha,beta]

class LogBetaBinomPDataGivenLAFCache(object):
    def __init__(self, vStar):
        self.vStar = vStar
        self.betaParams = []
        for lafbin in xrange(numLAFbins):
            self.betaParams.append( analytic_calc_alpha_beta(binLAF[lafbin], vStar) )
        self.cache = {}

    def compute(self, count1,count2,lafbin,vStar):
        n = count1+count2
        if n == 0:
            return 0
        if n < 0:
            raise Exception("HUNH?  Bad total count")
        if vStar != self.vStar:
            raise Exception('vStar mismatch')
        if (count1,count2,lafbin) not in self.cache:
            bp = self.betaParams[lafbin]
            result = logsum(my_log_betaBinomProb(n,count1,bp[0],bp[1]),
                            my_log_betaBinomProb(n,count2,bp[0],bp[1])) + math.log(.5)
            self.cache[(count1,count2,lafbin)] = result
        return self.cache[(count1,count2,lafbin)]
betaCache = None


def my_logBetaBinomPDataGivenLAF(count1,count2,lafbin,vStar):
    global betaCache
    if betaCache is None:
        betaCache = LogBetaBinomPDataGivenLAFCache(vStar)
    return betaCache.compute(count1,count2,lafbin,vStar)


def hetSnpLine():
    if mvVT != "snp":
        return False
    if mvZyg != "het-ref":
        return False
    return True


def line_is_useful():
    if mvTRC == "" or int(mvTRC) == 0:
        return False

    nreads = int(mvA1RC)+int(mvA2RC)

    if nreads < minNormalHetCount or nreads > maxNormalHetCount:
        #print "   ... skipped" 
        return False

    if mvA1VQ != "VQHIGH" or mvA2VQ != "VQHIGH":
        return False

    #print "   ... useful" 
    return True



def init_window(inf,out):
    global winChr
    global winBegin
    global winEnd
    global inputCnvLine
    
    winEnd = winBegin
    while winChr != mvChr or winEnd < int(mvBeg):
        inputCnvLine = inf.readline()
        if inputCnvLine == '':
            raise Exception("ERROR: CNV file exhausted before end of masterVar file")
        w = inputCnvLine.split('\t')
        winChr = w[0]
        winBegin = int(w[1])
        winEnd = int(w[2])
        if winChr != mvChr or winEnd < int(mvBeg):
            output_window(out)

def consume_tail_end_windows(inf,out):
    global winChr
    global winBegin
    global winEnd
    global inputCnvLine
    
    winEnd = winBegin
    inputCnvLine = inf.readline()
    while inputCnvLine != '':
        w = inputCnvLine.split('\t')
        winChr = w[0]
        winBegin = int(w[1])
        winEnd = int(w[2])
        output_window(out)
        inputCnvLine = inf.readline()
    

def output_window(out):

    global logL

    bestLogL = -1e200
    bestlaf = -1
    bestlafidx = -1
    lowlaf = -1
    highlaf = -1
    ttlLogL = -1e200
    for i in range(0,numLAFbins):
        ttlLogL = logsum(ttlLogL,logL[i])
        if bestLogL < logL[i]:
            bestLogL = logL[i]
            bestlaf = binLAF[i]
            bestlafidx = i

    i = 0
    j = numLAFbins-1
    tailsLogL = -1e200
    while i <= j and ( i < bestlafidx or j > bestlafidx ):
        
        if logL[i] <= logL[j] and i < bestlafidx and logsum(tailsLogL,logL[i]) < ttlLogL + math.log(1.-confidenceInterval):
            tailsLogL = logsum(tailsLogL,logL[i])
            i += 1
        else:
            if j > bestlafidx and logsum(tailsLogL,logL[j]) < ttlLogL + math.log(1.-confidenceInterval):
                tailsLogL = logsum(tailsLogL,logL[j])
                j -= 1
            else:
                break
            
    lowlaf = binLAF[i]
    highlaf = binLAF[j]

    out.write(inputCnvLine.rstrip('\n') + sep + str(bestlaf) + sep + str(lowlaf) + sep + str(highlaf) + '\n')
        
    global winBegin
    winBegin = -1

    for i in range(0,numLAFbins):
        logL[i] = 0.

    

def consumeData(inf,out,vStar):
    if winBegin == -1:
        init_window(inf,out)

    bestll = -1e200
    bestidx=-1
    
    if mvZyg == "het-ref":
        count1 = int(mvA1RC)
        count2 = int(mvA2RC)
        for i in range(0,numLAFbins):
            ll = my_logBetaBinomPDataGivenLAF(count1,count2,i,vStar)
            logL[i] += ll
            if ll > bestll:
                bestll = ll
                bestidx = i



def set_up_LAFbins():
    global binLAF
    global logL
    
    for i in range(1,numLAFbins+1):
        binLAF += [ float(i)/(float(numLAFbins)*2.) ]
        logL += [0.]


def massage_type(line,out):
    w = line.rstrip('\n').split('\t')
    if w[0] != '#TYPE' or len(w) > 2:
        raise Exception("massage_type called on bad line:\n" + line)
    
    oldType = w[1]
    newType = ''
    if oldType == 'CNV-SEGMENTS':
        newType = 'DIPLOID-SOMATIC-CNV-SEGMENTS'
    if oldType == 'TUMOR-CNV-SEGMENTS':
        newType = 'NONDIPLOID-SOMATIC-CNV-SEGMENTS'
    if oldType == 'CNV-DETAIL-SCORES':
        newType = 'DIPLOID-SOMATIC-CNV-DETAIL-SCORES'
    if oldType == 'TUMOR-CNV-DETAILS':
        newType = 'NONDIPLOID-SOMATIC-CNV-DETAILS'

    if newType == '':
        raise Exception("massage_type called on unknown #TYPE:\n" + oldType + '\n')

    out.write('#TYPE\t' + newType + '\n')


def addLAF(masterVar,output,cnvInput,vStar):

    set_up_LAFbins()
    
    mv = DelimitedFile(masterVar)
    mv.addField('chromosome')
    mv.addField('begin')
    mv.addField('end')
    mv.addField('zygosity')
    mv.addField('varType')
    mv.addField('allele1ReadCount')
    mv.addField('allele2ReadCount')
    mv.addField('totalReadCount')
    mv.addField('allele1VarQuality')
    mv.addField('allele2VarQuality')
    global mvChr
    global mvBeg
    global mvEnd
    global mvZyg
    global mvVT
    global mvA1RC
    global mvA2RC
    global mvTRC
    global mvA1VQ
    global mvA2VQ

    inf = openFileByExtension(cnvInput)
    out = open(output,'w')

    l = inf.readline()
    while l[0] != '>':
        if l[0:5] != '#TYPE':
            out.write(l)
        else:
            massage_type(l,out)
        l = inf.readline()

    out.write(l.rstrip('\n') + '\tbestLAF\tlowLAF\thighLAF\n')    

    for (mvChr,mvBeg,mvEnd,mvZyg,mvVT,mvA1RC,mvA2RC,mvTRC,mvA1VQ,mvA2VQ) in mv:
        if hetSnpLine():
            if line_is_useful():
                if mvChr != winChr or (mvBeg != None and winEnd < int(mvBeg)):
                    if winChr != "" and winBegin != None:
                        output_window(out)
                consumeData(inf,out,vStar)
                                  
    if winChr != "" and winBegin != None:
        output_window(out)

    consume_tail_end_windows(inf,out)

    
            
        

def main():
    parser = optparse.OptionParser('usage: %prog [options]')
    parser.set_defaults(masterVar='',output='',cnvInput='',
                        normalPvalCutoff='0.2',minNormal='10',maxNormal='300',vStar='.0075',
                        )
    parser.add_option('-m', '--master-var', dest='masterVar',
                      help='location of master var file')
    parser.add_option('-c', '--cnv-input', dest='cnvInput',
                      help='input file name (CNV segments or CNV details)')
    parser.add_option('-o', '--output', dest='output',
                      help='output file name')
    parser.add_option('--normal-pvalue-cutoff',dest='normalPvalCutoff',
                      help='cutoff to exclude normal-genome het sites with allele bias strong enough to ' +
                           'give this p-value or less for a two-sided binomial test against the 50/50 null hypothesis')
    parser.add_option('--normal-min-read-count',dest='minNormal',
                      help='minimum number of reads in normal sample for a normal-het site to be included for LAF estimation')
    parser.add_option('--normal-max-read-count',dest='maxNormal',
                      help='maximum number of reads in normal sample for a normal-het site to be included for LAF estimation')
    parser.add_option('--variance-fifty-fifty',dest='vStar',
                      help='variance of betaBinomial for equal allele fractions')


    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error('unexpected arguments')

    if not options.masterVar:
        parser.error('master-var specification required')
            
    if not options.output:
        parser.error('output specification required')
    if not options.cnvInput:
        parser.error('cnv-input specification required')

    global normalPvalueCutoff
    normalPvalueCutoff = float(options.normalPvalCutoff)
    global minNormalHetCount
    minNormalHetCount = int(options.minNormal)
    global maxNormalHetCount
    maxNormalHetCount = int(options.maxNormal)

    addLAF(options.masterVar,options.output,options.cnvInput,float(options.vStar))


if __name__ == '__main__':
    main()
    
