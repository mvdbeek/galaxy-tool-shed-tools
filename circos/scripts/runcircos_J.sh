#!/bin/bash

# usage runcircos_J.sh <conffile> <junctions file> <WG outfile> <montage outfile> <chromosome list> <build> <custom_outfile> <custum_region>

# chromosome_list is a comma separated list of chromosomes (e.g. 1,5,X,21) of those chromosomes we want a separate plot for

if [ $# -ne 9 ]
then
	echo "error, unexpected number of arguments in $0"
	exit
fi



picsdir="images"
mkdir images
conffile=$1
infile=$2
wg_out=$3
montage_out=$4
per_chr_file=$5
build=$6
custom_outfile=$7
custom_region=$8
filetype=$9

echo ""
echo "runcircos_J.sh: "
echo "conffile: $conffile, infile: $infile, wg_out: $wg_out, montage_out: $montage_out "
echo ""

fname=`basename ${infile}`
echo "file = ${infile}" > file.txt

if [[ $build != "hg18" && $build != "hg19" ]]
then
	echo "error, unknown build"
	exit
fi

if [ $build=="hg18" ]
then
	echo "karyotype = data/karyotype/karyotype.human.hg18.txt" > karyotype.txt
fi

if [ $build=="hg19" ]
then
	echo "karyotype = data/karyotype/karyotype.human.hg19.txt" > karyotype.txt
fi


echo "chromosomes_units = 1000000" > units.txt


##default settings
echo "show = yes" > showinter.txt
echo "show = yes" > showintra.txt
echo "color = black" > colorintra.txt
echo "thickness = 1" >> colorintra.txt
echo "color = red" > colorinter.txt
echo "thickness = 4" >> colorinter.txt


#####   Whole Genome plot
if [ ${wg_out} != "None" ] 
then

	#suppress ImpactedGenes track	
	mv showgenes.txt showgenes.txt.bak
	echo "show = no" > showgenes.txt

	echo "Making whole-genome plot"
	echo "chromosomes_display_default = yes" > chromosomes.txt
	echo "color = black" > colorintra.txt
	echo "thickness = 1" >> colorintra.txt

	echo "color = red" > colorinter.txt
	echo "thickness = 4" >> colorinter.txt

	echo "label_size = 30" > labelsize.txt
	echo "show = yes" > showinter.txt
	echo "show = yes" > showintra.txt
	echo "bezier_radius = 0r" >> showinter.txt	

	#run circos			
	circos -${filetype} -conf ${conffile} 

	#rename and move files
	mv circos_J.${filetype} ${wg_out}
	echo "mv circos_J.${filetype} ${wg_out}"	

	#reset ImpactedGenes track
	rm showgenes.txt
	mv showgenes.txt.bak showgenes.txt	
	
fi


#####  Per-chromosome images
if [[ ${montage_out} != "None" || ${per_chr_file} != "None" ]]
then
	echo "Making per-chromosome plots"
	echo "color = red" > colorinter.txt
	echo "thickness = 4" >> colorinter.txt
	echo "color = black" > colorintra.txt
	echo "thickness = 4" >> colorintra.txt
	echo "label_size = 75" > labelsize.txt

	for c in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y
	do
			echo "chromosomes_display_default = no" > chromosomes.txt
			echo "chromosomes = hs$c" >> chromosomes.txt
			echo "show = yes" > showintra.txt
			echo "bezier_radius = 0r" >> showintra.txt

			# run circos			
			circos -${filetype} -conf ${conffile}

			if [[ $c == "X" || $c == "Y" || $c -ge 10 ]]
			then  
				# rename and move files
				mv circos_J.${filetype} ${picsdir}/chr$c.${filetype}
			else 
				# rename and move files
				mv circos_J.${filetype} ${picsdir}/chr0$c.${filetype}
				
			fi


	done

	#combine per-chromosome images into 1, remove per-chromosome images
	if [ ${montage_out} != "None" ]
	then	
		echo "combining chromosome plots into montage"
		montage -quality 100 -density 3000  -geometry -10-10 ${picsdir}/chr* "${montage_out}.${filetype}"
		mv "${montage_out}.${filetype}" ${montage_out}
	fi
	
	#zip per-chromosome plots into single archive
	if [ ${per_chr_file} != "None" ]
	then	
		tar -czf ${per_chr_file} ${picsdir}/chr*
	fi

fi



#####  Custom region
if [ $custom_outfile != "None" ]
then
	echo "Making Custom region plot"
	echo "chromosomes_units = 1" > units.txt
	echo "chromosomes_display_default = no" > chromosomes.txt
	echo "chromosomes = ${custom_region}" >> chromosomes.txt		
	
	echo "show = yes" > showinter.txt
	echo "bezier_radius = 0r" >> showinter.txt	
	echo "color = red" > colorinter.txt
	echo "thickness = 4" >> colorinter.txt

	echo "show = yes" > showintra.txt
	echo "bezier_radius = 0r" >> showintra.txt
	echo "color = black" > colorintra.txt
	echo "thickness = 4" >> colorintra.txt

	

	# run circos			
	circos -${filetype} -conf ${conffile}

	# move output
	mv circos_J.${filetype} ${custom_outfile}
fi

echo "finished"



