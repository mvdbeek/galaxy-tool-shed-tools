#!/bin/bash

# usage runcircos_JSV.sh <conffile> <junctions file> <probes file> <snps file> <WG outfile> <montage outfile> <chromosome list> <build> <variations file> <variations file chr> <per-chr file> <custom_outfile> <custum_region> <filetype>

control_c()
# run if user hits control-c
{
  exit $?
}

if [ $# -ne 15 ]
then
	echo "error, unexpected number of arguments ($#) expected 15"
	echo "$@"
	exit
fi

picsdir="images"
mkdir images
conffile=$1
junctions_file=$2
probes_file=$3
snps_file=$4
wg_out=$5
montage_out=$6
per_chr_file=$7
build=$8
variations=$9
variations_chr=${10}
custom_outfile=${11}
custom_region=${12}
filetype=${13}
snps_original=${14}
probes_original=${15}

echo "head original snps"
head $snps_original

echo "head original probes"
head $probes_original


echo ""
echo "runcircos_JSV.sh: "
echo -e "conffile: $conffile\n junctionsfile: $junctions_file\n probes_file= $probes_file\n snps_file= $snps_file\n variations file= $variations \nwg_out: $wg_out\n montage_out: $montage_out \n"
echo ""

fname=`basename ${junctions_file}`
touch snp_maxval_all.txt
touch snp_maxval.txt

echo "file = ${junctions_file}" > junctionsfile.txt
echo "file = ${probes_file}" > probesfile.txt
echo "file = ${snps_file}" > snpsfile.txt
echo "file = ${variations}" > snpdensity.txt

#cat junctionsfile.txt
#cat probesfile.txt
#cat snpsfile.txt
#cat snpdensity.txt

if [[ $build != "hg18" && $build != "hg19" ]]
then
	echo "error, unknown build"
	exit
fi

if [ $build=="hg18" ]
then
	echo "karyotype = data/karyotype/karyotype.human.hg18.txt" > karyotype.txt
fi

if [ $build=="hg19" ]
then
	echo "karyotype = data/karyotype/karyotype.human.hg19.txt" > karyotype.txt
fi

echo "chromosomes_units = 1000000" > units.txt


##default settings
echo "show = yes" > showinter.txt
echo "show = yes" > showintra.txt
echo "color = black" > colorintra.txt
echo "thickness = 1" >> colorintra.txt
echo "color = red" > colorinter.txt
echo "thickness = 4" >> colorinter.txt





##############################   
#
#     Whole Genome plot
#
##############################
if [ ${wg_out} != "None" ] 
then

	#suppress ImpactedGenes track	
	mv showgenes.txt showgenes.txt.bak
	echo "show = no" > showgenes.txt

	echo "chromosomes_display_default = yes" > chromosomes.txt
	echo "color = black" > colorintra.txt
	echo "thickness = 1" >> colorintra.txt

	echo "color = red" > colorinter.txt
	echo "thickness = 4" >> colorinter.txt

	echo "label_size = 30" > labelsize.txt
	echo "show = yes" > showinter.txt
	echo "show = yes" > showintra.txt
	echo "bezier_radius = 0r" >> showinter.txt	

	echo "glyph_size = 4" > glyph.txt
	echo "thickness = 1" > varthickness.txt
	echo "thickness = 4" > junctions_thickness_inter.txt
	echo "thickness = 1" > junctions_thickness_intra.txt
	
	if [ $variations != "None" ]
	then
		cat snp_maxval_all.txt > snp_maxval.txt
	else	
		touch snp_maxval.txt	
	fi
	
	#run circos		
	echo ""
	echo "generating whole-genome plot"	
	echo ""
	circos -${filetype} -conf ${conffile} 

	#rename and move files
	echo "copying WG plot to galaxy output: mv circosJSV.${filetype} ${wg_out}"
	convert circosJSV.${filetype} -resize 50% ${wg_out}

	#reset ImpactedGenes track
	rm showgenes.txt
	mv showgenes.txt.bak showgenes.txt	

fi	

##############################   
#
#     Chromosome Plots
#
##############################
if [[ ${montage_out} != "None" || ${per_chr_file} != "None" ]]
then

	echo "color = black" > colorintra.txt
	echo "thickness = 4" >> colorintra.txt
	echo "label_size = 75" > labelsize.txt
	echo "glyph_size = 4" > glyph.txt
    echo "thickness = 3" > varthickness.txt
	echo "thickness = 6" > junctions_thickness_inter.txt
	echo "thickness = 3" > junctions_thickness_intra.txt
	echo "file = ${variations_chr}" > snpdensity.txt
	
	
	echo ""
	echo "generating per-chromosome plots"	
	echo ""

	#split snps and probes file per chromosoms
	if [[ $probes_original != "None" ]]
	then
		awk 'BEGIN{}{print $0 > "probes_"$1".txt"}END{}' $probes_original
	fi
	
	if [[ $snps_original != "None" ]]
	then
		awk 'BEGIN{}{print $0 > "snps_"$1".txt"}END{}' $snps_original
	fi
	
	for c in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y
	do
	
			echo "chromosomes_display_default = no" > chromosomes.txt
			echo "chromosomes = hs$c" >> chromosomes.txt
			echo "show = yes" > showintra.txt
			echo "bezier_radius = 0r" >> showintra.txt
			if [ $variations != "None" ]
			then
				cat snp_maxval_chr$c.txt > snp_maxval.txt			
			fi
			
			#get only current chromosome from snps and probes file, reduce number of points if necessary
			chromval="hs$c"
			echo "chromval $chromval"
			
						
			if [[ $snps_file != "None" ]]
			then
				cp "snps_${chromval}.txt" snpsfile_chrom.txt
				echo "file = snpsfile_chrom.txt" > snpsfile.txt
			
				#reduce number of points if necessary
				numpoints=`wc -l snpsfile_chrom.txt | cut -d" " -f1 `
				if [ $numpoints -gt 25000 ]
				then
					float_in=${numpoints}/25000
					ceil_val=${float_in/.*}
					ceil_val=$[$ceil_val+1]
			
					echo "reducing number of datapoints in probes file ($numpoints)"
					awk 'BEGIN{
							FS="\t"
							OFS="\t"	
							xlines="'"$ceil_val"'"
						}{
							if(FNR%xlines==1)
								print $0
				
						}END{}' snpsfile_chrom.txt > snps_circos_reduced		
					mv snps_circos_reduced	snpsfile_chrom.txt		
				fi
			fi
			
			#probes
			if [[ $probes_file != "None" ]]
			then
				cp "probes_${chromval}.txt" probesfile_chrom.txt
				echo "file = probesfile_chrom.txt" > probesfile.txt
				numpoints=`wc -l probesfile_chrom.txt | cut -d" " -f1 `
				if [ $numpoints -gt 25000 ]
				then
					float_in=${numpoints}/25000
					ceil_val=${float_in/.*}
					ceil_val=$[$ceil_val+1]
			
					echo "reducing number of datapoints in probes file ($numpoints)"
					awk 'BEGIN{
							FS="\t"
							OFS="\t"	
							xlines="'"$ceil_val"'"
						}{
							if(FNR%xlines==1)
								print $0
				
						}END{}' probesfile_chrom.txt > probes_circos_reduced		
					mv probes_circos_reduced	probesfile_chrom.txt	
				fi
											
			fi
			
			# run circos			
			circos -${filetype} -conf ${conffile}

			if [[ $c == "X" || $c == "Y" || $c -ge 10 ]]
			then  
				# rename and move files
				convert circosJSV.${filetype} -resize 50% ${picsdir}/chr$c.${filetype}
			else 
				# rename and move files
				convert circosJSV.${filetype} -resize 50% ${picsdir}/chr0$c.${filetype}
				
			fi


	done

	#combine per-chromosome images into 1, remove per-chromosome images
	if [ ${montage_out} != "None" ]
	then	
		echo "combining chromosome plots into montage"
		montage -quality 50 -density 3000 -limit memory 10GB -geometry -10-10 ${picsdir}/chr* "${montage_out}.${filetype}"
		convert "${montage_out}.${filetype}" -resize 30% ${montage_out}
		#mv "${montage_out}.${filetype}" ${montage_out}

	fi
	
	#zip per-chromosome plots into single archive
	if [ ${per_chr_file} != "None" ]
	then	
		tar -czf ${per_chr_file} ${picsdir}/chr*
	fi

fi



##############################   
#
#     Custom Region Plot
#
##############################
if [ $custom_outfile != "None" ]
then
	#exit
	echo "Making Custom region plot"
	echo "region: ${custom_region}"
	
	#enter only necessary data in snps and probes files
	awk 'BEGIN{}{print $0 > "probes_"$1".txt"}END{}' $probes_original
	awk 'BEGIN{}{print $0 > "snps_"$1".txt"}END{}' $snps_original
	
	touch probes_customregion.txt
	touch snps_customregion.txt
	

	for c in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 X Y
	do
		if [[ ${custom_region} =~ "hs$c"  ]]
		then
			echo "grabbing chr $c"
			cat probes_hs$c.txt >> probes_customregion.txt
			cat snps_hs$c.txt >> snps_customregion.txt 
		fi
	done
	
	head probes_customregion.txt
	head snps_customregion.txt
	#if files to big, downsample
	numpoints=`wc -l probes_customregion.txt | cut -d" " -f1 `
	if [ $numpoints -gt 25000 ]
	then
		float_in=${numpoints}/25000
		ceil_val=${float_in/.*}
		ceil_val=$[$ceil_val+1]
			
		echo "reducing number of datapoints in probes file ($numpoints)"
		awk 'BEGIN{
				FS="\t"
				OFS="\t"	
				xlines="'"$ceil_val"'"
			}{
				if(FNR%xlines==1)
					print $0
		
			}END{}' probes_customregion.txt > probes_customregion_reduced	
		rm probes_customregion.txt	
		mv probes_customregion_reduced	probes_customregion.txt		
	fi
	
	numpoints=`wc -l snps_customregion.txt | cut -d" " -f1 `
	if [ $numpoints -gt 25000 ]
	then
		float_in=${numpoints}/25000
		ceil_val=${float_in/.*}
		ceil_val=$[$ceil_val+1]
			
		echo "reducing number of datapoints in probes file ($numpoints)"
		awk 'BEGIN{
				FS="\t"
				OFS="\t"	
				xlines="'"$ceil_val"'"
			}{
				if(FNR%xlines==1)
					print $0
		
			}END{}' snps_customregion.txt > snps_customregion_reduced	
		rm snps_customregion.txt	
		mv snps_customregion_reduced	snps_customregion.txt		
	fi
	
	echo "file = probes_customregion.txt" > probesfile.txt
	echo "file = snps_customregion.txt" > snpsfile.txt
	
	#set custom region	
	echo "chromosomes_display_default = no" > chromosomes.txt
	echo "chromosomes = ${custom_region}" >> chromosomes.txt	
	
	#set colour and thickness
	echo "color = black" > colorintra.txt
	echo "thickness = 1" >> colorintra.txt
	echo "color = red" > colorinter.txt
	echo "thickness = 4" >> colorinter.txt

	echo "label_size = 30" > labelsize.txt
	echo "show = yes" > showinter.txt
	echo "show = yes" > showintra.txt
	echo "bezier_radius = 0r" >> showinter.txt	

	echo "glyph_size = 2" > glyph.txt
	echo "thickness = 1" > varthickness.txt
	echo "thickness = 4" > junctions_thickness_inter.txt
	echo "thickness = 1" > junctions_thickness_intra.txt
	if [ $variations != "None" ]
	then
		cat snp_maxval_all.txt > snp_maxval.txt
	fi
	echo "file = ${variations}" > snpdensity.txt

	
	# run circos		
	echo "starting circos"		
	circos -${filetype} -conf ${conffile}

	# move output
	echo "moving output"
	mv circosJSV.${filetype} ${custom_outfile}
fi

echo "finished"
		


