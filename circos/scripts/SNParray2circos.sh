#! /bin/bash

# usage: $0 <probes file> <snps file> <probes_out> <snps_out>

if [ $# -ne 4 ]
then
	echo "error, unexpected number of arguments in $0"
	exit
fi

probes=$1
snps=$2
probes_out=$3
snps_out=$4

echo ""
echo "$0: "
echo "probes: $probes, snps: $snps, probes_out: $probes_out, snps_out: $snps_out"
echo ""

# convert probes file	
if [ $probes != "None" ]
then
	sed '1,1d' $probes > $probes_out.tmp
	sed -i "s/chr/hs/g" $probes_out.tmp
	sed -i '/NaN/d' $probes_out.tmp
	sed -i '/hsMT/d' $probes_out.tmp
	sed -i '/hsM/d' $probes_out.tmp
	cut  -f1,2,3,4 $probes_out.tmp > $probes_out
fi

# convert snps file
if [ $snps != "None" ]
then
	sed '1,1d' $snps > $snps_out
	sed -i "s/chr/hs/g" $snps_out
	sed -i '/NaN/d' $snps_out
	sed -i '/hsMT/d' $snps_out
	sed -i '/hsM/d' $snps_out
fi


