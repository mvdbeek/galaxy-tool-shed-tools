# 1.4 LINKS AND RULES
#  
# The first data track we will add are links. By using rules, which
# are expressions that are evaluated for every link, the formatting
# can be dynamically changed based on data values.
# 
# I will also show you how to change the definition of colors, if you
# would like to assign different chromosome color scheme to your
# figure.


<image>
	dir   = .
	file  = circosJSV
	png   = yes
	svg   = yes

	# radius of inscribed circle in image
	radius         = 1500p

	# by default angle=0 is at 3 o'clock position
	angle_offset      = -90

	#angle_orientation = counterclockwise

	auto_alpha_colors = yes
	auto_alpha_steps  = 5

</image>


<<include karyotype.txt>>
<<include units.txt>>
<<include chromosomes.txt>>



#############################
#
#         Junctions 
#
#############################
<links>
	<link>		
		<<include junctionsfile.txt>>
		<<include show_junctions.txt>>
		radius        = 0.75r
		bezier_radius = 0.5r
		color         = black_a4
		thickness	  = 2
		ribbon        = no
		twist         = no

		# Rule blocks can be added to any <link> or <plot> block and form a
		# decision chain that changes how data points (e.g. links, histogram
		# bins, scatter plot glyphs, etc) are formatted.

		<rules>
			<rule>
				condition     = var(intrachr)			# all intrachromosomal	
				radius        = 0.75r
				color	      = black
				show          = yes
				bezier_radius = 0.2r
				flow          = continue
				<<include junctions_thickness_intra.txt>>

			</rule>

			<rule>			
				condition     = var(interchr)	
				radius        = 0.5r				
				color         = red		
				bezier_radius = 0r
				show          = yes		
				flow          = continue
				<<include junctions_thickness_inter.txt>>
			</rule>
		</rules>
	</link>
</links>


<plots>

	#############################
	#
	#         SmallVars
	#
	#############################

	#histogram
	<plot>

		<<include show_variations.txt>>
		type  = histogram
		
		<<include snpdensity.txt>>		
		color = dblue
		extend_bin = yes
		thickness = 4		

		min   = 0
		<<include snp_maxval.txt>>		
		r0    = 0.80r
		r1    = 0.95r
		  	
		<backgrounds>
			<background>
				color     = vvlgrey				
			</background>
		</backgrounds>

		<axes>		
		  thickness = 1
		  color = grey
		  <axis>
				spacing   = 0.25r		
		  </axis>
		</axes>

	 </plot>


	#############################
	#
	#         Impacted Genes
	#
	#############################	
	<plot>
		<<include showgenes.txt>>
		type             = text
		color            = red
		file             = ImpactedGenes.tsv

		show_links     = yes
		link_dims      = 4p,4p,8p,4p,4p
		link_thickness = 2p
		link_color     = black

		background_color = transparent
		label_size   = 24p
		label_font   = condensed

		padding  = 0p
		rpadding = 0p
		label_snuggle             = no
		# shift label up to its height in pixels in the angular direction
		max_snuggle_distance      = 2r
		snuggle_sampling          = 2
		snuggle_tolerance         = 0.25r
		snuggle_link_overlap_test = yes 
		snuggle_link_overlap_tolerance = 2p
		snuggle_refine            = yes

		r0 = 1.02r
		r1 = 1.38r
	</plot>

</plots>



# includes


<ideogram>

<spacing>

default = 0.0025r
break   = 0.5r

</spacing>


show_ticks          = yes
show_tick_labels    = no


show_bands            = yes
fill_bands            = yes
band_stroke_thickness = 6
band_stroke_color     = white
band_transparency     = 4


# Ideogram position, fill and outline
radius           = 0.85r
thickness        = 40p
fill             = yes
stroke_color     = dgrey
stroke_thickness = 2p

# Minimum definition for ideogram labels.

show_label       = yes
# see etc/fonts.conf for list of font names
label_font       = default 
label_radius     = dims(image,radius) - 120p
label_size = 75
label_parallel   = yes

</ideogram>

show_ticks          = yes
show_tick_labels    = no

<ticks>
radius           = 1r
color            = black
thickness        = 2p

# the tick label is derived by multiplying the tick position
# by 'multiplier' and casting it in 'format':
# 
# sprintf(format,position*multiplier)
# 

multiplier       = 1e-6

# %d   - integer
# %f   - float
# %.1f - float with one decimal
# %.2f - float with two decimals
# 
# for other formats, see http://perldoc.perl.org/functions/sprintf.html

format           = %d

<tick>
spacing        = 5u
size           = 10p
</tick>

<tick>
spacing        = 25u
size           = 15p
show_label     = yes
label_size     = 20p
label_offset   = 10p
format         = %d
</tick>

</ticks>

<<include etc/colors_fonts_patterns.conf>> 

<<include etc/housekeeping.conf>> 
