#!/bin/bash

# somaticplot.sh $junctions $mastervar $superlocusoutput $cnvdetails $cnvsegments $gender $outputplot $legend

if [ $# -ne 9 ]
then
    echo "$0 error: unexpected number of arguments"	
    exit   
fi

echo "somaticplot.sh $1 $2 $3 $4 $5 $6 $7 $8"

junctions=$1
masterVar=$2
superlocusoutput=$3
cnvdetails=$4
cnvsegments=$5
sampleType=$6 
outputplot=$7
legend=$8  
circospackage=$9

# location of scripts for Circos generation and related helper computes
circosutils="$circospackage/bin"

# location of Complete Genomics Circos plot conf files 
confDir="$circospackage/circosConf"

    
plotType=somatic
plotLabel="somaticplot"

#remove chrM
sed -i '/chrM/d' $junctions
sed -i '/chrM/d' $superlocusoutput
sed -i '/chrM/d' $masterVar
sed -i '/chrM/d' $cnvdetails
sed -i '/chrM/d' $cnvsegments


#create temporary directory
#base=$RANDOM
#echo "random: ${base}"
outDir=`pwd`
#mkdir $outDir



echo "${circosutils}/circosPlot.py \
	--junction-file ${junctions} \
	--masterVar-file ${masterVar} \
	--calldiff-super-locus ${superlocusoutput} \
	--circos-output-dir ${outDir} \
	--plot-label ${plotLabel} \
	--sample-type ${sampleType} \
	--cnv-details ${cnvdetails} \
	--cnv-segments ${cnvsegments} \
	--conf-dir ${confDir} \
	--plot-type ${plotType}"

### construct Circos inputs, copy required conf files, run Circos,
### and create html wrapper around resulting png files
${circosutils}/circosPlot.py \
	--junction-file ${junctions} \
	--masterVar-file ${masterVar} \
	--calldiff-super-locus ${superlocusoutput} \
	--circos-output-dir ${outDir} \
	--plot-label "${plotLabel}" \
	--sample-type ${sampleType} \
	--cnv-details ${cnvdetails} \
	--cnv-segments ${cnvsegments} \
	--conf-dir ${confDir} \
	--plot-type ${plotType}

#rename output files
mv "${outDir}/circos-${plotLabel}.png" ${outputplot}
mv "${outDir}/circosLegend.png" ${legend}


