#!/bin/bash

#######################################################################################
######### The following commands (sh or bash shell script) illustrate how to  #########
######### generate a 'somatic' Circos plot similar to those created by the    #########
######### Complete Genomics pipeline (releases 2.0 and 2.1 currently).        #########
#######################################################################################
    ########################
    ### Prerequisites    ###
    ### 1) python        ###
    ### 2) circos        ###
    ### 3) cgatools      ###
    ########################
    ###################################################################################
    ### What needs to be modified:                                                  ###
    ### Lines immediately below the comments "RUN-SPECIFIC" or "USER-SITE-SPECIFIC" ###
    ### need to be modified.                                                        ###
    ### - Lines below "RUN-SPECIFIC" must be adjusted appropriately for each run.   ###
    ### - Lines below "USER-SITE-SPECIFIC" must be adjusted to reflect the user's   ###
    ###   site (paths to binaries etc) but can be left alone from run to run.       ###
    ###################################################################################
    
    
### type of Circos plot
plotType=somatic

### location of non-baseline (typically tumor) genome
##  Note that this 'root' specification is just a 
##  convenience used in this example, with the assumption
##  that it points to an intact Complete Genomics export
##  package.  It is possible to specify the locations of
##  all files individually if the file layout has been
##  modified.
##  RUN-SPECIFIC
rootA=/HCC2218/ASM/HCC2218-H-200-37-ASM/B/EXP/package/GS00258-DNA_B03

### location of baseline (typically normal) genome
##  The note above 'rootA' applies here as well.
##  RUN-SPECIFIC
rootB=/HCC2218/ASM/HCC2218-H-200-37-ASM/A/EXP/package/GS00258-DNA_A03

### location of crr file (must be correct build [36 or 37]; male can be used regardless of sample sex)
##  RUN-SPECIFIC
refcrr=/home/user/ref/build37.crr

### output destination; create directory if not already present
##  RUN-SPECIFIC
outDir=/home/user/CircosOutput/Somatic_HCC2218
if test ! -e $outDir; then mkdir $outDir; fi

### non-baseline masterVar file
masterVarA=`ls ${rootA}/ASM/masterVar*tsv.bz2`

### baseline masterVar file
masterVarB=`ls ${rootB}/ASM/masterVar*tsv.bz2`

### non-baseline cnvDetailsNondiploid file (used for LAF estimates)
cnvDetailsFile=`ls ${rootA}/ASM/CNV/somaticCnvDetailsNondiploidBeta*tsv.bz2`

### non-baseline CNV segments file
cnvSegmentsFile=`ls ${rootA}/ASM/CNV/somaticCnvSegmentsNondiploidBeta*tsv`

### high confidence somatic junctions file
highConfSomJcns=`ls ${rootA}/ASM/SV/somaticHighConfidenceJunctionsBeta*tsv`

### sample type, based on sex and reference build
##  RUN-SPECIFIC
sampleType=female37

### label to be used in html page wrapping Circos plots
##  RUN-SPECIFIC
plotLabel=somatic-HCC2218

### location of scripts for Circos generation and related helper computes
##  USER-SITE-SPECIFIC
circosutils=/home/user/circosPackage/


### location of Complete Genomics Circos plot conf files
##  USER-SITE-SPECIFIC
confDir=/home/user/circosPackage/


### path to Circos installation
##  USER-SITE-SPECIFIC
circosbin=/home/user/bin/circos-0.52/bin
export PATH=${circosbin}:${PATH}

### location of cgatools
##  USER-SITE-SPECIFIC
cgatoolsbin=/cgatools/bin
export PATH=${cgatoolsbin}:${PATH}


### run cgatools calldiff with somatic comparison to provide somatic track information
##  somatic Circos plot requires the SuperlocusOutput file from calldiff
cgatools calldiff \
--reference "${refcrr}" \
--variantsA "${masterVarA}" \
--variantsB "${masterVarB}" \
--output-prefix "${outDir}/" \
--reports "SuperlocusOutput" \
--beta

### construct Circos inputs, copy required conf files, run Circos,
### and create html wrapper around resulting png files
${circosutils}/circosPlot.py \
	--junction-file ${highConfSomJcns} \
	--masterVar-file ${masterVarA} \
	--calldiff-super-locus ${outDir}/SuperlocusOutput.tsv \
	--circos-output-dir ${outDir} \
	--plot-label ${plotLabel} \
	--sample-type ${sampleType} \
	--cnv-details ${cnvDetailsFile} \
	--cnv-segments ${cnvSegmentsFile} \
	--conf-dir ${confDir} \
	--plot-type ${plotType}
