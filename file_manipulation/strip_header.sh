 #!/bin/bash
 
inputfile=$1
outputfile=$2
header=$3
commentchar=$4

echo "commentchar: -${commentchar}-"
 
sed -e "/^${commentchar}/d" -e '/^$/d' $inputfile > $outputfile
sed -n "/^${commentchar}/p"  $inputfile > $header 	
