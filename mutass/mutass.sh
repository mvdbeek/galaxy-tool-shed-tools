#!/bin/bash

## s.hiltemann@erasmusmc.nl
## 

# command: mutass.sh $reference $infile $col_chr $col_pos $col_ref $col_obs $oufile
# $reference: type: string options: "hg18"/"hg19" description: genome build
# $col_chr: type: int description: number of column containing chromosome of variant
# $col_pos: type: int
# $col_ref: type: int
# $col_obs: type: int
# $col_chr: type: int

#echo "input parameters: $@"
if [ $# -ne 8 ]
then
	echo "unexpected number of arguments in $0, exiting"
	exit
fi

build=$1
infile=$2
chr_col=$3
pos_col=$4
ref_col=$5
sub_col=$6
outfile=$7
cgatools=$8
numfields="error"

mutassString="http://mutationassessor.org/?cm=var&fts=all&frm=txt&"
varString="var="

#create version of infile with linenumbers as variant IDs (so we can join file back together later)
awk 'BEGIN{
		FS="\t"
		OFS="\t"
	}{
		if(FNR==1)
			print "tmpvarId", $0
		else
			print FNR,$0

}END{}' $infile > infile_numbered


numfields=`awk 'BEGIN{FS="\t"}{}END{print NF}' infile_numbered`
#echo "numfields in file: $numfields"

chr_col=$3
pos_col=$4
ref_col=$5
sub_col=$6

chr_col=$[$chr_col+1]
pos_col=$[$pos_col+1]
ref_col=$[$ref_col+1]
sub_col=$[$sub_col+1]

Id_col=1


#for every line, run mutationassessor
echo "error" > mutass_all 
linenum=0;

#echo "running Mutass"
# put lines into array	
IFS=$'\n'
linesarray=($(cat infile_numbered)) 
	
# for each line, if already annotated with sift and polyphen, query mutation assessor    
for line in ${linesarray[@]}
do     
			
	chr=`echo $line | cut -d "	" -f $chr_col`
	pos=`echo $line | cut -d "	" -f $pos_col` 
	ref=`echo $line | cut -d "	" -f $ref_col`
	sub=`echo $line | cut -d "	" -f $sub_col`		
	ID=`echo $line | cut -d "	" -f $Id_col`
		
	#remove chr from chromosome if present
	chr=${chr/chr/}

	#echo "chr: $chr, pos: $pos, ref: $ref, sub: $sub, sift: $sift_score, pph2: $pph2_score"
	varString="var=$build,$chr,$pos,$ref,$sub"
	wget -q -O mutass.variant "${mutassString}${varString}"
		
	#put output from mutass into file
	if [ $linenum -eq 1 ]
	then
		echo -e "tmpvarId	chromosome	position	reference	observed	`sed -n '1p' mutass.variant`" > mutass_all
		echo -e "${ID}	chr${chr}	${pos}	${ref}	${sub}	`sed -n '2p' mutass.variant`" >> mutass_all
	else				
		echo -e "${ID}	chr${chr}	${pos}	${ref}	${sub}	`sed -n '2p' mutass.variant`" >> mutass_all		
	fi

	# be polite, wait a bit till next request
	sleep 0.3		
		
	linenum=$[$linenum+1]
	
done 

#head mutass_all

cut -f1,8- mutass_all > mutass_almost

#head mutass_almost

#join mutass annotations with original file
#echo "joining condel scores with original file"
$cgatools join --beta \
	--input infile_numbered mutass_almost \
	--output mutass_joined \
	--match tmpvarId:tmpvarId \
	--select A.*,B.* \
	-a \
	-m compact	

#remove our ID columns (first column and first after all original columns)
next=$[$numfields+2]
#echo "next: $next"
cut -f2-${numfields},${next}- mutass_joined > $outfile	



