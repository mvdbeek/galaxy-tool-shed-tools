#!/bin/bash

gc5base=$1     #gc5Base file
normalfile=$2
tumourfile=$3
filetype=$4  #bam or pileup
sequenzadir=$5


###########################
#
#  Input file conversion
#
###########################


if [ $filetype == "bam" ]
then
	#convert to pileup and pipe to sequenza converter
	mkfifo normal.fifo tumour.fifo
	samtools mpileup -Q 20 ${normalfile} > normal.fifo &
	samtools mpileup -Q 20 ${tumourfile} > tumour.fifo &
	${sequenzadir}/sequenza-utils.py pileup2abfreq -gc ${gc5base} \
								-r normal.fifo \
								-s tumour.fifo | gzip > out.abfreq.gz	
	rm normal.fifo tumour.fifo							
	
elif [ ${filetype} == "pileup" ]  # input is pileup files	
then
	${sequenzadir}/sequenza-utils.py pileup2abfreq -gc ${gc5base} \
								-r ${normalfile} \
								-s ${tumourfile} | gzip > out.abfreq.gz	
else #already abfreq 
	gzip ${normalfile} > out.abfreq.gz
fi 



########################
#
#    Run Sequenza
#
########################

Rscript ${sequenzadir}/sequenza.R out.abfreq.gz



