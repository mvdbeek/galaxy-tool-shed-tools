# INSTALLATION:
# source("http://bioconductor.org/biocLite.R")
# biocLite("copynumber")
# install.packages("sequenza")
#

# Fetch commandline arguments
args <- commandArgs(trailingOnly = TRUE)
abfreqfile <- args[1]  # expected to be gzipped file


library("sequenza")
cat('Sequenza run\n')



# BAM/pileup file must be preprocessed (not from R) using sequenze-utils.py. Location of this script can be determined as follows:
system.file("exec", "sequenza-utils.py", package="sequenza")

# load ABfreq file (output of sequenza-utils.py script)
#data.file <- system.file("data", "abf.data.abfreq.txt.gz", package="sequenza")
data.file <- abfreqfile
data.file

#read all data
abf.data <- read.abfreq(data.file)
str(abf.data, vec.len=2)

# GC normalization
# Gather GC-content informatin form the entire file and in the meantime map the chromosome position in the file (for fast acces later) 
gc.stats <- gc.sample.stats(data.file)
str(gc.stats)

# Normalization to the depth.ratio
gc.vect <- setNames(gc.stats$raw.mean, gc.stats$gc.values)
abf.data$adjusted.ratio <- abf.data$depth.ratio / gc.vect[as.character(abf.data$GC.percent)]
par(mfrow = c(1,1), cex = 1, las = 1, bty = 'l')
matplot(gc.stats$gc.values, gc.stats$raw, type='b', col=1, pch=c(1,19,1), lty=c(2,1,2), xlab='GC content(%)', ylab='Uncorrected depth ratio')
legend('topright', legend=colnames(gc.stats$raw), pch=c(1,19,1))
hist2(abf.data$depth.ratio, abf.data$adjusted.ratio, breaks=prettyLog, key=vkey, panel.first=abline(0,1,lty=2),xlab='Uncorrected depth ratio',ylab='GC-adjusted depth ratio')

# Extract information from ABfreq file
test <- sequenza.extract(data.file)
names(test)


# Plot before correction for estimated ploidy and cellularity
# Plots of Mutation (top), B-allele frequencies (middle) and depth ratio (bottom) for each chromosome
#for (i in 1:23){
#	chromosome.view(mut.tab = test$mutations[[i]], baf.windows = test$BAF[[i]], ratio.windows=test$ratio[[i]], min.N.ratio = 1, segments=test$segments[[i]], main=test$chromosomes[i])
#}

# After the raw data is conveniently processed, we can apply the Bayesian inference implemented 
# in the packag. The function sequenza.fit performs the inference using the calculated B allele
# frequency and depth ration of the obtained segment. The method can be explored in more detail
# by looking at the manual pages for the function baf.model.fit
CP.example <- sequenza.fit(test)

# The object resulting from sequenza.fit have two vectors, x and y, indicating respectively the tested
# values of ploidy and cellularity, and a matrix z with x columns and y rows, containing the estimated
# log-likelihood. Confidence intervals for these two parameters can be calculated using function get.ci
cint <- get.ci(CP.example)

# It is also possible to plot the likelihood over the combinations of the two parameters, highlighting 
# the point estimate and confidence region
cp.plot(CP.example)
cp.plot.contours(CP.example, add=TRUE, likThresh = c(0.999))


# By exploring the result for cellularity and ploidy separately, it is possible to draw the likelihood
# distribution for each parameter. The information is returned by the get.ci function.
#par(mfrow=c(3,2))
par(mfrow=c(1,1), mar=c(7,4,4,2))
cp.plot(CP.example, legend.pos="topright")
cp.plot.contours(CP.example, legend.pos="topright", add=TRUE)

plot(cint$values.y, ylab="Cellularity", xlab="likelihood", type="n", main=paste("Predicted Cellularity: ",cint$max.y))
select <- cint$confint.y[1] <= cint$values.y[,2] & cint$values.y[,2] <= cint$confint.y[2]
polygon(y=c(cint$confint.y[1], cint$values.y[select,2], cint$confint.y[2]), x=c(0,cint$values.y[select, 1],0), col='red', border=NA)
lines(cint$values.y)
abline(h=cint$max.y, lty=2, lwd=0.5)

plot(cint$values.x, xlab="Ploidy",ylab="likelihood",type="n", main=paste("Predicted Ploidy: ",cint$max.x))
select <- cint$confint.x[1] <= cint$values.x[,1] & cint$values.x[,1] <= cint$confint.x[2]
polygon(x=c(cint$confint.x[1], cint$values.x[select,1], cint$confint.x[2]), y=c(0,cint$values.x[select, 2],0), col='red', border=NA)
lines(cint$values.x)
abline(v=cint$max.x, lty=2, lwd=0.5)


# Call CNVs and mutation using the estimated parameters

cellularity <- cint$max.y
cellularity

ploidy <- cint$max.x
ploidy

# In addition, we need to calculate the average normalized depth ratio, used to set a value for the baseline copy number
avg.depth.ratio <-mean(test$gc$adj[,2])

#Detect variant alleles (mutations)

mut.tab <- na.exclude(do.call(rbind, test$mutations))
mut.alleles <- mufreq.bayes(mufreq=mut.tab$F, depth.ratio=mut.tab$adjusted.ratio, cellularity=cellularity, ploidy=ploidy, avg.depth.ratio=avg.depth.ratio)
head(mut.alleles)


# Detect copy number variations
seg.tab <- na.exclude(do.call(rbind, test$segments))
cn.alleles <- baf.bayes(Bf=seg.tab$Bf, depth.ratio=seg.tab$depth.ratio, cellularity=cellularity, ploidy=ploidy, avg.depth.ratio=avg.depth.ratio)
head(cn.alleles)
seg.tab <- cbind(seg.tab, cn.alleles)
head(seg.tab)

# Visualize detetcted copy number changes and variant alleles
for (i in 1:23){
	chromosome.view(mut.tab=test$mutations[[i]], baf.windows=test$BAF[[i]], ratio.windows=test$ratio[[i]], min.N.ratio=1, segments=seg.tab[seg.tab$chromosome == test$chromosomes[i],], main=test$chromosomes[i], cellularity=cellularity, ploidy=ploidy, avg.depth.ratio=avg.depth.ratio)
}

#genome-wide view of the alleles and copy number states
par(mfrow=c(1,1), mar=c(5,4,4,2))
genome.view(seg.cn=seg.tab, info.type="CNt")
legend("bottomright", bty="n", c("Tumour copy number"), col=c("red"), inset=c(0,-0.4), pch=15, xpd=TRUE)

genome.view(seg.cn=seg.tab, info.type="AB")
legend("bottomright", bty="n", c("A-allele","B-allele"), col=c("red","blue"), inset=c(0,-0.45), pch=15, xpd=TRUE)




