#/bin/bash

# ifuse_rscript.sh $fileType $inputFile $genomeBuild $outputTSV \$IFUSE_PATH
filetype=$1
inputfile=$2
build=$3
output=$4
output_fg=$5
ifusepath=$6


# conversion of fusionmap input
echo "### Converting input files"
if [ $filetype == "fusionmap" ]
then
	${ifusepath}/fusionmap2ifuse.sh $inputfile Rinput.tsv

else # CG junctions file, just remove header
	cp $inputfile Rinput.tsv
	sed -i '/^#/d ' Rinput.tsv
	sed -i '/^$/d ' Rinput.tsv
fi

if [ $build == "hg18" ]
then
	genesfile=${ifusepath}/ucscgeneshg18.txt
else
	genesfile=${ifusepath}/ucscgeneshg19.txt
fi

# call Rscript
echo "### Running Annotation R script"
Rscript ${ifusepath}/ifuse.R Rinput.tsv $output $genesfile 2>&1


awk 'BEGIN{
		FS="\t"
		OFS="\t"
	}{
		if (FNR==1)
		 	print $0
		 else if ($5=="yes" && $7=="same orientation"){
		 	print $0
		 }
	
	}END{}' ${output} > ${output_fg}








