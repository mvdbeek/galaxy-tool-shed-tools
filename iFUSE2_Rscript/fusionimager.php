<?php
include("ifuseloader.php");
include("svg_gene.php");
include("sequenceloader.php");
include("ifusefilevalidator.php");

if(empty($argv[1]) || empty($argv[2]) || empty($argv[3]))
{
	print "Usage:\nphp ".$argv[0]." <filename> <outdir> <refdir>\n";
	exit(1);
}

$filename = $argv[1];
if (!file_exists($filename))
{
    throw new Exception('Input file not found '.$filename);
}
$outdir = $argv[2];
$ref = $argv[3];


// for testing; like print_r() but with limited depth
// source: some comment somewhere at php.net
function print_array($array, $depth = 1, $indentation = 0)
{
	if (is_array($array))
	{
		echo "Array(\n";
		foreach ($array as $key=>$value)
		{
			if(is_array($value))
			{
				if($depth)
				{
					//echo "max depth reached.\n";
				}
				else
				{
					for($i = 0; $i < $indentation; $i++)
					{
						echo "    ";
					}
					echo $key."=Array(";
					print_array($value, $depth-1, $indentation+1);
					for($i = 0; $i < $indentation; $i++)
					{
						echo "    ";
					}
					echo ");";
				}
			}
			else
			{
				for($i = 0; $i < $indentation; $i++)
				{
					echo "    ";
				}
				echo $key."=>".$value."\n";
			}
		}
		echo ");\n";
	}
	else
	{
		echo "Not an array\n";
	}
}

// -------------------------------------------------------------

$loader = new ifuseloader($filename, $ref);
$data  =& $loader->getData(); # calls sequenceloader->concat_sequences

$svgmaker = new svg_gene();

$count = 0;
foreach($data as $item)
{
	$count++;
	
	$svg = '<?xml version="1.0" encoding="utf-8"?>';
	$svg .= preg_replace("/[\n\r\t]/",'',$svgmaker->getSVG($svgmaker->getGeneInformationSVG($item))); #TODO refactor method call
	
	file_put_contents (  $outdir.'svg'.$count.'.svg' ,  $svg);
	
	//$image=new imagick(); 
	//$image->readImageBlob($svg);
	//$image->setImageFormat("png32");
	
	//print_array($item, 1, 1);
	
	// write image
	// TODO use Related.Junctions as filename to test, and print error if file already exists for testing
	//$fh = fopen($outdir."test1.png", "w");
	//fwrite($fh, $image);
	//fclose($fh);
	
	//if($count >= 1) break; # for testing
}



?>
