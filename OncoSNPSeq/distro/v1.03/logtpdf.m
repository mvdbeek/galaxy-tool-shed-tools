function [y, u] = logtpdf(x, m, S, v)

[p, n] = size(x);

z = x - m;

d = (1/S)*sum(z.*z, 1);

y = -0.5*log(S) - 0.5*(v+p)*log(1 + d/v);
u = (v + p)./(v + d);
