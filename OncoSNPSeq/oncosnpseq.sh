#!/bin/bash

function usage(){
	echo "usage: $0 todo"
}

#    run_oncoseq.sh $MCRDIR \
#				--maxcopy 10 \
#				--read_depth_range $read_depth_range \
#				--chr_range "[1:22]" \
#				--n_train 30000 \
#				--maxploidy $maxploidy \
#				--minploidy $minploidy \
#				$normalcontamination \
#				--tumourheterogeneity \
#				--tumourstatestable $tumourstatestable  \
#				--maxnormalcontamination $maxnormalcontamination \
#				--hgtable $hgtable \
#				--seqtype $seqtype \
#				--infile $infile \			

echo "oncosnpseq.sh start"
echo "parameters: $@"

#set some defaults
normalcont="N"
tumourheterogeneity="N"
tumourstates="None"
hgtab="None"
seqtype="cg"
infileN="None"

				
set -- `getopt -n$0 -u -a --longoptions="maxcopy: readdepthrange: chrrange: ntrain: maxploidy: minploidy: MCRDIR: ONCODIR: seqtype: maxnormalcont: infileT: infileN: normalcontamination: tumourstatestable: hgtable: tumourheterogeneity: newfilepath: graphicsout: graphicsid:" "h:" "$@"` || usage
[ $# -eq 0 ] && usage

while [ $# -gt 0 ]
do
	echo "processing option: $1 $2"
    case "$1" in
    	--newfilepath)	   		newfilepath=$2;shift;;  
    	--graphicsid)	   		graphicsid=$2;shift;;  
    	--graphicsout)	   		graphicsout=$2;shift;;  
       	--maxcopy)	     		maxcopy=$2;shift;;  
		--readdepthrange)		read_depth_range=$2;shift;; 
		--chrrange)				chr_range=$2;shift;;  	
		--ntrain)				n_train=$2;shift;;
		--maxploidy)			maxploidy=$2;shift;;
		--minploidy)			minploidy=$2;shift;;
		--MCRDIR)				MCRDIR=$2;shift;;
		--ONCODIR)				ONCODIR=$2;shift;;
		--maxnormalcont) 		maxnormalcontamination=$2;shift;;
		--seqtype)				seqtype=$2;shift;;
		--infileN)				infileN=$2;shift;;
		--infileT)				infileT=$2;shift;;
		--normalcontamination)	normalcont=$2;shift;;
		--tumourheterogeneity)	tumourhet=$2;shift;;
		--tumourstatestable)	tumourstates=$2;shift;;
		--hgtable)				hgtab=$2;shift;;
        -h)        	shift;;
		--)        	shift;break;;
        -*)        	usage;;
        *)         	break;;            
    esac
    shift
done

samplename="galaxy"

echo "maxcopy: $maxcopy"
echo "ONCODIR: $ONCODIR"
echo "MCRDIR: $MCRDIR"
echo "infile T: $infileT"
echo "infile N: $infileN"


#process some parameters
if [[ $normalcont == "Y" ]]
then
	normalcontamination="--normalcontamination"
else
	normalcontamination=""
fi

#process some parameters
if [[ $tumourhet == "Y" ]]
then
	tumourheterogeneity="--tumourheterogeneity"
else
	tumourheterogeneity=""
fi

if [[ $tumourstates == "None" ]]
then
	tumourstatestable=${ONCODIR}/distro/v1.03/config/tumourStates.txt
	cp $tumourstatestable tumour_states.txt
else	
	cp ${ONCODIR}/distro/v1.03/config/tumourStates.txt tumour_states.txt
	tumourstatestable=$tumourstates
fi

if [[ $hgtab == "None" ]]
then
	hgtable=${ONCODIR}/distro/v1.03/config/hgTables_b37.txt
	cp $tumourstatestable hgtable.txt
else
	cp ${ONCODIR}/distro/v1.03/config/hgTables_b37.txt hgtable.txt
	hgtable=$hgtab
fi


# convert input 
echo "converting input file(s)"
if [ $seqtype == "cg" ]
then
	if [ $infileN != "None" ]
	then
		echo "paired: perl ${ONCODIR}/process_mastervar_standard_versus_normal.pl --infile $infileT --normalfile $infileN --outfile input_converted.tsv"
		perl ${ONCODIR}/process_mastervar_standard_versus_normal.pl --infile $infileT --normalfile $infileN --outfile input_converted.tsv
	else
		echo "unpaired: perl ${ONCODIR}/process_mastervar.pl --infile $infileT --outfile input_converted.tsv"
		perl ${ONCODIR}/process_mastervar.pl --infile $infileT --outfile input_converted.tsv
	fi
	
fi
head input_converted.tsv
# TODO: illumina input conversion

infile=input_converted.tsv

#run oncosnpseq 
echo "starting oncosnpseq"
echo "tumour states table: $tumourstatestable"
echo "command:"
echo "${ONCODIR}/distro/v1.03/executables/run_oncoseq.sh $MCRDIR \n
		--maxcopy $maxcopy \n
		--read_depth_range [${read_depth_range}] \n
		--chr_range [${chr_range}] \n
		--n_train $n_train \n
		--maxploidy $maxploidy \n
		--minploidy $minploidy \n
		$normalcontamination \n
		$tumourheterogeneity \n
		--tumourstatestable $tumourstatestable  \n
		--maxnormalcontamination $maxnormalcontamination \n
		--hgtable $hgtable \n
		--seqtype $seqtype \n
		--samplename $samplename \n
		--infile $infile \n
		--outdir `pwd` \n"



${ONCODIR}/distro/v1.03/executables/run_oncoseq.sh $MCRDIR --read_depth_range "[${read_depth_range}]" --chr_range "[${chr_range}]" --n_train $n_train --maxploidy $maxploidy --minploidy $minploidy 		$normalcontamination $tumourheterogeneity --tumourstatestable $tumourstatestable --maxnormalcontamination $maxnormalcontamination --hgtable $hgtable --seqtype $seqtype --samplename $samplename --infile $infile --outdir `pwd`

#renaming output
ls

if [ -f galaxy.1.ps.gz ]
then
	gunzip galaxy.1.ps.gz
	ps2pdf galaxy.1.ps $graphicsout
fi

trynum=2
try="Y"
while [ $try == "Y" ]
do	
	if [ -f galaxy.${trynum}.ps.gz ]
	then
		echo "graphics file ${trynum} found"
		gunzip galaxy.${trynum}.ps.gz
		#ps2pdf galaxy.${trynum}.ps galaxy_graphics${trynum}.pdf
		ps2pdf galaxy.${trynum}.ps ${newfilepath}/primary_${graphicsid}_graphics${trynum}_visible_pdf
		trynum=$[$trynum+1]
	else
		try="N"	
	fi
done

if [ -f galaxy.cost.ps ]
then	
	ps2pdf galaxy.cost.ps galaxy_cost.pdf
fi

echo "oncosnpseq.sh end"
