Installs VirtualNormal Correction Tool

After installing this tool via admin panel, manually configure the following:

1) edit virtual_normal_correction.loc file (in tool-data directory)

   - change "/path/to/hg18.crr" to the location of the Complete Genomics reference crr file on your system
     (can be downloaded from ftp://ftp.completegenomics.com/ReferenceFiles/ )
     
   - change "/path/to/VN_genomes_varfiles_hg18.txt" to the location of the file containing the locations of all the Complete Genomics
     varfiles to be used as a virtual normal. This file should contain 1 file location per line, e.g.
     
	    /path/to/normal-varfile-1
		/path/to/normal-varfile-2
		/path/to/normal-varfile-3
		/path/to/normal-varfile-4
		/path/to/normal-varfile-5
		/path/to/normal-varfile-6
		/path/to/normal-varfile-7
		/path/to/normal-varfile-8
   			...
   			
   	 Varfiles can be in compressed or uncompressed form. For example, Complete Genomics' Diversity panel can be used.
     (can be downloaded from ftp://ftp2.completegenomics.com/)		
   
   - change	"/path/to/VN_genomes_junctionfiles_hg18.txt" to the location of the file containing the locations of all the Complete Genomics
     junctionfiles to be used as a virtual normal. This file should contain 1 file location per line.  For example, Complete Genomics' 
	 Diversity panel can be used. (can be downloaded from ftp://ftp2.completegenomics.com/)
     
 2) restart Galaxy for changes to take effect
 
 After this initial setup, additional normals can be added to the lists without having to restart Galaxy. 
