# 	fusionmap.sh
#				--reference $ref.reference 
#				--genemodel $genemodel
#				--ftype $inputtype.itype 
#				--input1 $input 
#				#if $inputtype.itype == "fastq":
#				--input2 $input2 
#				#endif
#				--rnamode $rnamode 
#				--outputfusionreads $outputfusionreads 
#				--pairedend $pairedend 
#				--reportout $FusionReport 
#				--bamout $FusionBAM

## default parameters
fmdir=$FUSIONMAP_DIR
refbuild="Human.hg19"
genebuild="RefGene"
rnamode="True"
gzip="false"
outputfusionreads="True"
PairedEnd="False"
MinimalHit=2
RealignToGenome="False"
echo "params: $@"

## get user parameters
set -- `getopt -n$0 -u -a --longoptions="reference: genemodel: ftype: input1: input2: rnamode: outputfusionreads: pairedend: reportout: bamout: MinimalHit: MinimalRescuedReadNumber: RealignToGenome" "h:" "$@"` || usage
[ $# -eq 0 ] && usage

while [ $# -gt 0 ]
do
    case "$1" in
       	--reference)      		refbuild=$2;shift;;
		--genemodel)			genebuild=$2;shift;;
		--ftype)				ftype=$2;shift;;
		--input1)				input1=$2;shift;;
		--input2)				input2=$2;shift;;
		--rnamode)				rnamode=$2;shift;;
		--outputfusionreads)	outputfusionreads=$2;shift;;
		--pairedend)			PairedEnd=$2;shift;;
		--reportout)			reportout=$2;shift;;
		--bamout)				bamout=$2;shift;;
		--MinimalHit)				MinimalHit=$2;shift;;
		--MinimalRescuedReadNumber)	MinimalRescuedReadNumber=$2;shift;;
		--RealignToGenome)			RealignToGenome=$2;shift;;
        -h)        	shift;;
	--)        	shift;break;;
        -*)        	usage;;
        *)         	break;;
    esac
    shift
done



## setup proper file names for input file (fusionmap requires it)
if [ $ftype == "fastq" ]
then
	ftype="FASTQ"
	cp $input1 "inputfile_1.fastq"
	input_one="inputfile_1.fastq"

	if [ $input2 != "None" ]
	then
		cp $input2 "inputfile_2.fastq"
		input_two="inputfile_2.fastq"
	fi

else # BAM
	cp $input1 "inputfile_1.bam"
	input_one="inputfile_1.bam"
	
	if [ $input2 != "None" ]
	then
		cp $input2 "inputfile_2.bam"
		input_two="inputfile_2.bam"
	fi
fi

if [ $PairedEnd == "Yes" ]
then
	reportype="--pereport"
else
	reporttype="--semap"
fi

# check installation
echo " ==> Running FusionMap"
echo " ..checking mono installation:"
which mono
echo " ..checking fusionmap installation:"
ls -la $FUSIONMAP_DIR/bin/FusionMap.exe


# create the config file for the run
touch fmconfig.txt
echo -e "<Files>" > fmconfig.txt
echo -e "$input_one" >> fmconfig.txt
if [[ $ftype == "FASTQ" && $input2 != "None" ]]
then
	echo -e "$input_two" >> fmconfig.txt
fi

echo -e "\n<Options>" >> fmconfig.txt
if [[ $ftype == "FASTQ" && $input2 != "None" ]]
then
	echo -e "PairedEnd=$PairedEnd" >> fmconfig.txt
fi
echo -e "RnaMode=$rnamode" >> fmconfig.txt
echo -e "ThreadNumber=4" >> fmconfig.txt
echo -e "FileFormat=$ftype" >> fmconfig.txt
echo -e "OutputFusionReads=$outputfusionreads" >> fmconfig.txt
echo -e "Gzip=$gzip" >> fmconfig.txt

echo -e "\n<Output>" >> fmconfig.txt
echo -e "TempPath=./tmpdir" >> fmconfig.txt
echo -e "OutputPath=./outputdir" >> fmconfig.txt
echo -e "OutputName=outfile" >> fmconfig.txt

# show config file
echo -e "\n ==> config file: "
cat fmconfig.txt


# run fusionmap
mkdir tmpdir
mkdir outputdir
mono $FUSIONMAP_DIR/bin/FusionMap.exe $reporttype $fmdir $refbuild $genebuild fmconfig.txt > /tmp/log.txt

## copy output files to galaxy outputs
cp outputdir/outfile.FusionReport.txt $reportout
cp outputdir/inputfile*.FusionReads.bam $bamout


echo -e "\n mono finished. Output files:"
ls outputdir/

echo -e "\n ==> FusionMap log file:"
cat /tmp/log.txt



